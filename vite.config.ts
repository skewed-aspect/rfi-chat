//----------------------------------------------------------------------------------------------------------------------

import dotenv from 'dotenv';
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import Components from 'unplugin-vue-components/vite';
import { BootstrapVueNextResolver } from 'bootstrap-vue-next';

// Interfaces
import { ServerConfig } from './src/common/interfaces/config';

// Utils
import configUtil from '@strata-js/util-config';

// ---------------------------------------------------------------------------------------------------------------------
// Configuration
// ---------------------------------------------------------------------------------------------------------------------

dotenv.config();
const env = (process.env.ENVIRONMENT ?? 'local').toLowerCase();
configUtil.load(`./config/${ env }.yml`);

const config = configUtil.get<ServerConfig>();

//----------------------------------------------------------------------------------------------------------------------

// https://vitejs.dev/config/
export default defineConfig({
    root: 'src/client',
    publicDir: 'assets',
    plugins: [
        vue(),
        Components({
            resolvers: [ BootstrapVueNextResolver() ],
        }),
    ],
    server: {
        host: config.http.host,
        port: config.http.port,
        proxy: {
            // Add additional routes here
            '/api': `http://localhost:${ config.http.port - 1 }`,
            '/version': `http://localhost:${ config.http.port - 1 }`,
            '/socket.io': {
                target: `http://localhost:${ config.http.port - 1 }`,
                ws: true,
            },
        },
    },
    build: {
        outDir: '../../dist/client',
        emptyOutDir: true,
        cssCodeSplit: true,
    },
});

//----------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// Test Setup
// ---------------------------------------------------------------------------------------------------------------------
import { resolve } from 'node:path';
import { rmSync } from 'node:fs';

import 'dotenv/config';
import configUtil from '@strata-js/util-config';
import logging from '@strata-js/util-logging';

// Utils
import { getDB } from '../src/server/utils/database';

// ---------------------------------------------------------------------------------------------------------------------

export async function mochaGlobalSetup() : Promise<void>
{
    // Load the configuration
    configUtil.load(`./config/mocha.yml`);

    // Set up logging
    logging.setConfig({ level: 'error', prettyPrint: true });

    // Delete the 'db/chat-test.db' file
    rmSync(resolve(__dirname, '../db/chat-test.db'), { force: true, recursive: true });

    // Set up the database
    const db = await getDB();

    // Run migrations
    await db.migrate.latest({
        directory: './src/server/knex/migrations',
    });

    // Run seeds
    await db.seed.run({
        directory: './src/server/knex/seeds',
    });
}

export async function mochaGlobalTeardown() : Promise<void>
{
    // Delete the 'db/chat-test.db' file
    rmSync(resolve(__dirname, '../db/chat-test.db'), { force: true, recursive: true });
}

// ---------------------------------------------------------------------------------------------------------------------

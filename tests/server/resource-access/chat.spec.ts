// ---------------------------------------------------------------------------------------------------------------------
// Unit Tests for the chat.spec.ts module.
// ---------------------------------------------------------------------------------------------------------------------

import { expect } from 'chai';

// Resource Access
import { get, list, create, addMember, removeMember, update, remove } from '../../../src/server/resource-access/chat';

// Util
import { getDB } from '../../../src/server/utils/database';

// ---------------------------------------------------------------------------------------------------------------------

describe('Chat Resource Access', () =>
{
    before(async () =>
    {
        // Get the database
        const db = await getDB();

        // Create a new chat
        await db('chat').insert({ id: 'chat1', name: 'Chat 1' });
        await db('chat').insert({ id: 'chat2', name: 'Chat 2' });

        // Add members to the chats
        await db('chat_member').insert({ chat_id: 'chat1', persona: 'wrenchGirl70' });
        await db('chat_member').insert({ chat_id: 'chat1', persona: 'KwisatzHatTrick' });

        await db('chat_member').insert({ chat_id: 'chat2', persona: 'AceHuntress62' });
        await db('chat_member').insert({ chat_id: 'chat2', persona: 'KwisatzHatTrick' });
    });

    after(async () =>
    {
        // Get the database
        const db = await getDB();

        // Delete the chat database
        await db('chat').truncate();
        await db('chat_member').truncate();
    });

    describe('get', () =>
    {
        it('should get a chat', async () =>
        {
            // Attempt to get `chat1`
            const chat = await get('chat1');

            // Check the chat
            expect(chat.id).to.equal('chat1');
            expect(chat.name).to.equal('Chat 1');
            expect(chat.members).to.have.length(2);
            expect(chat.members[0]).to.equal('wrenchGirl70');
            expect(chat.members[1]).to.equal('KwisatzHatTrick');
        });

        it('should throw an error when no chat is found', async () =>
        {
            const results = await get('unknownChat');
            expect(results).to.be.null;
        });
    });

    describe('list', () =>
    {
        it('should list all chats', async () =>
        {
            // Attempt to list all chats
            const chats = await list();

            // Check the chats
            expect(chats).to.have.length(2);
            expect(chats[0].id).to.equal('chat1');
            expect(chats[0].name).to.equal('Chat 1');
        });

        it('should list all chats for a persona', async () =>
        {
            // Attempt to list all chats for `KwisatzHatTrick`
            const chats = await list('KwisatzHatTrick');

            // Check the chats
            expect(chats).to.have.length(2);
            expect(chats[0].id).to.equal('chat1');
            expect(chats[0].name).to.equal('Chat 1');
            expect(chats[1].id).to.equal('chat2');
            expect(chats[1].name).to.equal('Chat 2');
        });
    });

    describe('create', () =>
    {
        it('should create a chat', async () =>
        {
            const newChat = {
                id: 'chat3',
                name: 'Chat 3',
                members: [ 'wrenchGirl70', 'AceHuntress62', 'KwisatzHatTrick' ],
            };

            // Create a new chat
            await create(newChat);

            // Attempt to get `chat3`
            const chat = await get('chat3');

            // Check the chat
            expect(chat.id).to.equal('chat3');
            expect(chat.name).to.equal('Chat 3');
            expect(chat.members).to.have.length(3);
        });

        it('should throw an error when creating a chat with the same id', async () =>
        {
            const newChat = {
                id: 'chat1',
                name: 'Chat 1',
                members: [ 'wrenchGirl70', 'AceHuntress62', 'KwisatzHatTrick' ],
            };

            try
            {
                await create(newChat);
                expect.fail('Should have thrown an error');
            }
            catch (error)
            {
                expect(error.message).to.equal('Chat with id \'chat1\' already exists.');
            }
        });
    });

    describe('addMember', () =>
    {
        it('should add a member to a chat', async () =>
        {
            // Add `AceHuntress62` to `chat1`
            await addMember('chat1', 'AceHuntress62');

            // Get the chat
            const chat = await get('chat1');

            // Check the chat
            expect(chat.members).to.have.length(3);
            expect(chat.members[2]).to.equal('AceHuntress62');
        });

        it('should throw an error when adding a member that already exists', async () =>
        {
            try
            {
                await addMember('chat1', 'wrenchGirl70');
                expect.fail('Should have thrown an error');
            }
            catch (error)
            {
                expect(error.message).to.equal('Member \'wrenchGirl70\' is already in chat \'chat1\'.');
            }
        });
    });

    describe('removeMember', () =>
    {
        it('should remove a member from a chat', async () =>
        {
            // Remove `KwisatzHatTrick` from `chat1`
            await removeMember('chat1', 'KwisatzHatTrick');

            // Get the chat
            const chat = await get('chat1');

            // Check the chat
            expect(chat.members).to.have.length(2);
            expect(chat.members[0]).to.equal('wrenchGirl70');
            expect(chat.members[1]).to.equal('AceHuntress62');
        });

        it('should not throw an error when removing a member that does not exist', async () =>
        {
            // Remove `unknownPersona` from `chat1`
            await removeMember('chat1', 'unknownPersona');
        });
    });

    describe('update', () =>
    {
        it('should update a chat', async () =>
        {
            // Update `chat1`
            await update({ id: 'chat1', name: 'New Chat 1', description: 'New Description'});

            // Get the chat
            const chat = await get('chat1');

            // Check the chat
            expect(chat.id).to.equal('chat1');
            expect(chat.name).to.equal('New Chat 1');
            expect(chat.description).to.equal('New Description');
        });

        it('should throw an error when updating a chat that does not exist', async () =>
        {
            try
            {
                await update({ id: 'unknownChat', name: 'Unknown Chat' });
                expect.fail('Should have thrown an error');
            }
            catch (error)
            {
                expect(error.message).to.equal('Chat with id \'unknownChat\' does not exist.');
            }
        });
    });

    describe('remove', () =>
    {
        it('should remove a chat', async () =>
        {
            // Remove `chat1`
            await remove('chat1');

            // Attempt to get `chat1`
            const results = await get('chat1');
            expect(results).to.be.null;
        });

        it('should not throw an error when the chat does not exist', async () =>
        {
            // Remove `unknownChat`
            await remove('unknownChat');
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------

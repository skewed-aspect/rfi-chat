// ---------------------------------------------------------------------------------------------------------------------
// Unit Tests for Chat Aliases Resource Access
// ---------------------------------------------------------------------------------------------------------------------

import { expect } from 'chai';

// Resource Access
import { get, list, create, update, remove } from '../../../src/server/resource-access/chatAliases';

// Util
import { getDB } from '../../../src/server/utils/database';

// ---------------------------------------------------------------------------------------------------------------------

describe('Chat Alias Resource Access', () =>
{
    before(async () =>
    {
        // Get the database
        const db = await getDB();

        // Create a new chat
        await db('chat').insert({ id: 'chat1', name: 'Chat 1' });
        await db('chat').insert({ id: 'chat2', name: 'Chat 2' });
        await db('chat').insert({ id: 'chat3', name: 'Chat 3' });

        // Add members to the chats
        await db('chat_member').insert({ chat_id: 'chat1', persona: 'wrenchGirl70' });
        await db('chat_member').insert({ chat_id: 'chat1', persona: 'KwisatzHatTrick' });

        await db('chat_member').insert({ chat_id: 'chat2', persona: 'AceHuntress62' });
        await db('chat_member').insert({ chat_id: 'chat2', persona: 'KwisatzHatTrick' });

        await db('chat_member').insert({ chat_id: 'chat3', persona: 'wrenchGirl70' });
        await db('chat_member').insert({ chat_id: 'chat3', persona: 'AceHuntress62' });
        await db('chat_member').insert({ chat_id: 'chat3', persona: 'KwisatzHatTrick' });

        // Create a new chat alias
        await db('chat_alias')
            .insert({
                chat_id: 'chat1',
                alias: 'chat-1',
                owner: 'wrenchGirl70',
            });

        await db('chat_alias')
            .insert({
                chat_id: 'chat2',
                alias: 'chat-2',
                owner: 'AceHuntress62',
            });
    });

    after(async () =>
    {
        // Get the database
        const db = await getDB();

        // Delete the chat database
        await db('chat')
            .truncate();

        // Delete the chat aliases database
        await db('chat_alias')
            .truncate();
    });

    describe('get', () =>
    {
        it('should get a chat alias', async () =>
        {
            // Attempt to get `chat1`
            const chatAlias = await get('chat1', 'wrenchGirl70');

            // Check the chat alias
            expect(chatAlias.chatID).to.equal('chat1');
            expect(chatAlias.owner).to.equal('wrenchGirl70');
            expect(chatAlias.alias).to.equal('chat-1');
        });

        it('should return null when no chat alias is found', async () =>
        {
            const results = await get('unknownChat', 'wrenchGirl70');
            expect(results).to.be.null;
        });
    });

    describe('list', () =>
    {
        it('should list all chat aliases', async () =>
        {
            // Get all chat aliases
            const chatAliases = await list();

            // Check the chat aliases
            expect(chatAliases).to.have.length(2);
            expect(chatAliases[0].chatID).to.equal('chat1');
            expect(chatAliases[0].owner).to.equal('wrenchGirl70');
            expect(chatAliases[0].alias).to.equal('chat-1');
        });

        it('should list all chat aliases for a persona', async () =>
        {
            // Get all chat aliases for `AceHuntress62`
            const chatAliases = await list('AceHuntress62');

            // Check the chat aliases
            expect(chatAliases).to.have.length(1);
            expect(chatAliases[0].chatID).to.equal('chat2');
            expect(chatAliases[0].owner).to.equal('AceHuntress62');
            expect(chatAliases[0].alias).to.equal('chat-2');
        });
    });

    describe('create', () =>
    {
        it('should create a chat alias', async () =>
        {
            // Create a new chat alias
            const chatAlias = {
                chatID: 'chat1',
                owner: 'AceHuntress62',
                alias: 'chat-1',
            };

            // Attempt to create the chat alias
            await create(chatAlias);

            // Get the chat alias
            const createdChatAlias = await get('chat1', 'AceHuntress62');

            // Check the chat alias
            expect(createdChatAlias.chatID).to.equal('chat1');
            expect(createdChatAlias.owner).to.equal('AceHuntress62');
            expect(createdChatAlias.alias).to.equal('chat-1');
        });

        it('should throw an error when creating a chat alias that already exists', async () =>
        {
            // Attempt to create a chat alias that already exists
            try
            {
                await create({
                    chatID: 'chat1',
                    owner: 'wrenchGirl70',
                    alias: 'chat-1',
                });
                expect.fail('Should have thrown an error');
            }
            catch (error)
            {
                expect(error.message).to.equal('A chat alias with chatID \'chat1\', owner \'wrenchGirl70\' already exists.');
            }
        });
    });

    describe('update', () =>
    {
        it('should update a chat alias', async () =>
        {
            // Update the chat alias
            await update({ chatID: 'chat1', owner: 'wrenchGirl70', alias: 'new-chat-1' });

            // Get the chat alias
            const updatedChatAlias = await get('chat1', 'wrenchGirl70');

            // Check the chat alias
            expect(updatedChatAlias.chatID).to.equal('chat1');
            expect(updatedChatAlias.owner).to.equal('wrenchGirl70');
            expect(updatedChatAlias.alias).to.equal('new-chat-1');
        });

        it('should throw an error when updating a chat alias that does not exist', async () =>
        {
            try
            {
                await update({ chatID: 'unknownChat', owner: 'wrenchGirl70', alias: 'new-chat-1' });
                expect.fail('Should have thrown an error');
            }
            catch (error)
            {
                expect(error.message)
                    .to
                    .equal('Chat alias with chatID \'unknownChat\', owner \'wrenchGirl70\' does not exist.');
            }
        });
    });

    describe('remove', () =>
    {
        it('should remove a chat alias', async () =>
        {
            // Remove `chat1`
            await remove('chat1', 'wrenchGirl70');

            // Attempt to get `chat1`
            const results = await get('chat1', 'wrenchGirl70');
            expect(results).to.be.null;
        });

        it('should not throw an error when the chat alias does not exist', async () =>
        {
            // Remove `unknownChat`
            await remove('unknownChat', 'wrenchGirl70');
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// Unit Tests for the personas.spec.ts module.
// ---------------------------------------------------------------------------------------------------------------------

import { expect } from 'chai';

// Resource Access
import { get, list, create, update, remove } from '../../../src/server/resource-access/personas';

// ---------------------------------------------------------------------------------------------------------------------

describe('Persona Resource Access', () =>
{
    it('should have a system persona', async () =>
    {
        // Attempt to get `system`
        const persona = await get('system');

        // Check the persona
        expect(persona.username).to.equal('system');
        expect(persona.fullName).to.equal('System');
        expect(persona.avatar).to.be.undefined;
        expect(persona.wikiPage).to.be.undefined;
    });

    describe('get', () =>
    {
        it('should get a persona', async () =>
        {
            // Attempt to get `wrenchGirl70`
            const persona = await get('wrenchGirl70');

            // Check the persona
            expect(persona.username).to.equal('wrenchGirl70');
            expect(persona.fullName).to.equal('Whitney Antares');
            expect(persona.avatar).to.not.be.undefined;
            expect(persona.avatar?.buffer).to.not.be.undefined;
            expect(persona.avatar?.mime).to.not.be.undefined;
            expect(persona.avatar?.mime).to.equal('image/png');
            expect(persona.wikiPage).to.equal('https://rfiuniverse.com/characters/whitney-antares');
        });

        it('should return null when no persona is found', async () =>
        {
            const result = await get('unknownPersona');
            expect(result).to.be.null;
        });
    });

    describe('list', () =>
    {
        it('should list all personas', async () =>
        {
            // Attempt to list all personas
            const personas = await list();

            // Check the personas
            expect(personas).to.have.length(4);
            expect(personas[0].username).to.equal('system');
            expect(personas[1].username).to.equal('wrenchGirl70');
        });
    });

    describe('create', () =>
    {
        it('should create a persona', async () =>
        {
            // Create a new persona
            const persona = {
                username: 'newUser',
                fullName: 'New User',
                avatar: {
                    buffer: Buffer.from('avatar'),
                    mime: 'image/jpeg',
                },
                wikiPage: 'https://rfiuniverse.com/characters/new-user',
            };

            // Attempt to create the persona
            await create(persona);

            // Get the persona
            const createdPersona = await get('newUser');

            // Check the persona
            expect(createdPersona.username).to.equal('newUser');
            expect(createdPersona.fullName).to.equal('New User');
            expect(createdPersona.avatar).to.not.be.undefined;
            expect(createdPersona.avatar?.buffer).to.not.be.undefined;
            expect(createdPersona.avatar?.mime).to.not.be.undefined;
            expect(createdPersona.avatar?.mime).to.equal('image/jpeg');
            expect(createdPersona.wikiPage).to.equal('https://rfiuniverse.com/characters/new-user');
        });

        it('should throw an error when a persona already exists', async () =>
        {
            // Attempt to create a persona that already exists
            try
            {
                await create({
                    username: 'system',
                    fullName: 'System',
                    avatar: undefined,
                    wikiPage: undefined,
                });
                expect.fail('Should have thrown an error');
            }
            catch (error)
            {
                expect(error.message).to.equal('A persona with username \'system\' already exists.');
            }
        });
    });

    describe('update', () =>
    {
        it('should update a persona', async () =>
        {
            // Update the `newUser` persona
            const persona = {
                username: 'newUser',
                fullName: 'New User Updated',
                avatar: {
                    buffer: Buffer.from('avatar updated'),
                    mime: 'image/jpeg',
                },
                wikiPage: 'https://rfiuniverse.com/characters/new-user-updated',
            };

            // Attempt to update the persona
            await update(persona);

            // Get the persona
            const updatedPersona = await get('newUser');

            // Check the persona
            expect(updatedPersona.username).to.equal('newUser');
            expect(updatedPersona.fullName).to.equal('New User Updated');
            expect(updatedPersona.avatar).to.not.be.undefined;
            expect(updatedPersona.avatar?.buffer).to.not.be.undefined;
            expect(updatedPersona.avatar?.mime).to.not.be.undefined;
            expect(updatedPersona.avatar?.mime).to.equal('image/jpeg');
            expect(updatedPersona.wikiPage).to.equal('https://rfiuniverse.com/characters/new-user-updated');
        });

        it('should throw an error when a persona does not exist', async () =>
        {
            // Attempt to update a persona that does not exist
            try
            {
                await update({
                    username: 'unknownPersona',
                    fullName: 'Unknown Persona',
                    avatar: undefined,
                    wikiPage: undefined,
                });
                expect.fail('Should have thrown an error');
            }
            catch (error)
            {
                expect(error.message).to.equal('No persona with username \'unknownPersona\' found.');
            }
        });
    });

    describe('remove', () =>
    {
        it('should remove a persona', async () =>
        {
            // Remove the `newUser` persona
            await remove('newUser');

            // Attempt to get the `newUser` persona
            const result = await get('newUser');

            // Check the result
            expect(result).to.be.null;
        });

        it('should not throw when the persona does not exist', async () =>
        {
            // Attempt to remove a persona that does not exist
            await remove('unknownPersona');
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------

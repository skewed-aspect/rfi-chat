// ---------------------------------------------------------------------------------------------------------------------
// Unit Tests for the personaAliases.spec.ts module.
// ---------------------------------------------------------------------------------------------------------------------

import { expect } from 'chai';

// Resource Access
import { get, list, create, update, remove } from '../../../src/server/resource-access/personaAliases';

// Util
import { getDB } from '../../../src/server/utils/database';

// ---------------------------------------------------------------------------------------------------------------------

describe('Persona Alias Resource Access', () =>
{
    before(async () =>
    {
        // Get the database
        const db = await getDB();

        // Create a new persona alias
        await db('persona_alias').insert({ target: 'wrenchGirl70', alias: 'full-whit', owner: 'AceHuntress62' });
        await db('persona_alias').insert({ target: 'KwisatzHatTrick', alias: 'brother', owner: 'AceHuntress62' });

        await db('persona_alias').insert({ target: 'AceHuntress62', alias: 'Yse', owner: 'wrenchGirl70' });
        await db('persona_alias').insert({ target: 'KwisatzHatTrick', alias: 'David', owner: 'wrenchGirl70' });
    });

    after(async () =>
    {
        // Get the database
        const db = await getDB();

        // Delete the persona aliases database
        await db('persona_alias').truncate();
    });

    describe('get', () =>
    {
        it('should get a persona alias', async () =>
        {
            // Attempt to get `wrenchGirl70`
            const personaAlias = await get('wrenchGirl70', 'AceHuntress62');

            // Check the persona alias
            expect(personaAlias.target).to.equal('wrenchGirl70');
            expect(personaAlias.owner).to.equal('AceHuntress62');
            expect(personaAlias.alias).to.equal('full-whit');
        });

        it('should return null when no persona alias is found', async () =>
        {
            const results = await get('unknownPersona', 'AceHuntress62');
            expect(results).to.be.null;
        });

        it('should return null when asking for an alias from an unknown persona', async () =>
        {
            const results = await get('wrenchGirl70', 'unknownPersona');
            expect(results).to.be.null;
        });
    });

    describe('list', () =>
    {
        it('should list all persona aliases', async () =>
        {
            // Attempt to list all persona aliases
            const personaAliases = await list();

            // Check the persona aliases
            expect(personaAliases.length).to.be.greaterThan(0);
        });

        it('should list all persona aliases for a persona', async () =>
        {
            // Attempt to list all persona aliases for `wrenchGirl70`
            const personaAliases = await list('wrenchGirl70');

            // Check the persona aliases
            expect(personaAliases.length).to.equal(2);
            expect(personaAliases[0].target).to.equal('AceHuntress62');
            expect(personaAliases[0].alias).to.equal('Yse');
            expect(personaAliases[1].target).to.equal('KwisatzHatTrick');
            expect(personaAliases[1].alias).to.equal('David');
        });
    });

    describe('create', () =>
    {
        before(async () =>
        {
            // Get the database
            const db = await getDB();

            // Delete the persona aliases database
            await db('persona').insert({ username: 'testPersona', full_name: 'Test Persona' });
        });

        after(async () =>
        {
            // Get the database
            const db = await getDB();

            // Delete the persona aliases database
            await db('persona')
                .where({ username: 'testPersona' })
                .del();
        });

        it('should create a persona alias', async () =>
        {
            // Create a new persona alias
            await create({ target: 'testPersona', alias: 'test', owner: 'wrenchGirl70' });

            // Attempt to get the new persona alias
            const personaAlias = await get('testPersona', 'wrenchGirl70');

            // Check the persona alias
            expect(personaAlias.target).to.equal('testPersona');
            expect(personaAlias.alias).to.equal('test');
        });

        it('should throw an error when a persona alias already exists', async () =>
        {
            try
            {
                await create({ target: 'testPersona', alias: 'test', owner: 'wrenchGirl70' });
                expect.fail('Should have thrown an error');
            }
            catch (error)
            {
                expect(error.message).to.equal('A persona alias with target \'testPersona\' and owner \'wrenchGirl70\' already exists.');
            }
        });
    });

    describe('update', () =>
    {
        it('should update a persona alias', async () =>
        {
            // Update the persona alias
            await update({ target: 'wrenchGirl70', alias: 'new-whit', owner: 'AceHuntress62' });

            // Get the persona alias
            const personaAlias = await get('wrenchGirl70', 'AceHuntress62');

            // Check the persona alias
            expect(personaAlias.target).to.equal('wrenchGirl70');
            expect(personaAlias.owner).to.equal('AceHuntress62');
            expect(personaAlias.alias).to.equal('new-whit');
        });

        it('should throw an error when updating a persona alias that does not exist', async () =>
        {
            try
            {
                await update({ target: 'unknownPersona', alias: 'unknown', owner: 'AceHuntress62' });
                expect.fail('Should have thrown an error');
            }
            catch (error)
            {
                expect(error.message).to.equal('No alias with target \'unknownPersona\' and owner \'AceHuntress62\' found.');
            }
        });
    });

    describe('remove', () =>
    {
        it('should remove a persona alias', async () =>
        {
            // Remove the persona alias
            await remove('wrenchGirl70', 'AceHuntress62');

            // Attempt to get the persona alias
            const result = await get('wrenchGirl70', 'AceHuntress62');

            // Check the persona alias
            expect(result).to.be.null;
        });

        it('should not throw when the persona alias does not exist', async () =>
        {
            // Remove the persona alias
            await remove('unknownPersona', 'AceHuntress62');
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------

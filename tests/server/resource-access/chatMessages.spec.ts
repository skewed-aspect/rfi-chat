// ---------------------------------------------------------------------------------------------------------------------
// Unit Tests for the chatMessages.spec.ts module.
// ---------------------------------------------------------------------------------------------------------------------

import { expect } from 'chai';

// Resource Access
import { get, list, create, edit, update, remove } from '../../../src/server/resource-access/chatMessages';

// Util
import { getDB } from '../../../src/server/utils/database';

// ---------------------------------------------------------------------------------------------------------------------

describe('Chat Message Resource Access', () =>
{
    before(async () =>
    {
        // Get the database
        const db = await getDB();

        // Create a new chat
        await db('chat').insert({ id: 'chat1', name: 'Chat 1' });
        await db('chat').insert({ id: 'chat2', name: 'Chat 2' });

        // Add members to the chats
        await db('chat_member').insert({ chat_id: 'chat1', persona: 'wrenchGirl70' });
        await db('chat_member').insert({ chat_id: 'chat1', persona: 'KwisatzHatTrick' });

        await db('chat_member').insert({ chat_id: 'chat2', persona: 'AceHuntress62' });
        await db('chat_member').insert({ chat_id: 'chat2', persona: 'KwisatzHatTrick' });

        // Create a new chat message
        await db('chat_message')
            .insert({
                chat_id: 'chat1',
                id: 'message1',
                author: 'wrenchGirl70',
                content: 'Hello, World!',
                created_at: new Date('2621-01-01T00:00:00.000Z'),
            });

        await db('chat_message')
            .insert({
                chat_id: 'chat1',
                id: 'message2',
                author: 'KwisatzHatTrick',
                content: 'Greetings!',
                created_at: new Date('2621-01-02T00:00:00.000Z'),
            });

        await db('chat_message')
            .insert({
                chat_id: 'chat1',
                id: 'message3',
                author: 'KwisatzHatTrick',
                content: 'Second Message.',
                created_at: new Date('2621-01-03T00:00:00.000Z'),
            });

        await db('chat_message')
            .insert({
                chat_id: 'chat2',
                id: 'message4',
                author: 'AceHuntress62',
                content: 'Greetings!',
                created_at: new Date('2621-01-01T00:00:00.000Z'),
            });

        await db('chat_message')
            .insert({
                chat_id: 'chat2',
                id: 'message5',
                author: 'KwisatzHatTrick',
                content: 'Salute!',
                created_at: new Date('2621-01-02T00:00:00.000Z'),
            });
    });

    after(async () =>
    {
        // Get the database
        const db = await getDB();

        // Delete the chat messages database
        await db('chat_message')
            .truncate();
    });

    describe('get', () =>
    {
        it('should get a chat message', async () =>
        {
            // Attempt to get `message1`
            const message = await get('message1');

            // Check the chat message
            expect(message.id).to.equal('message1');
            expect(message.author).to.equal('wrenchGirl70');
            expect(message.content).to.equal('Hello, World!');
        });

        it('should throw an error when no chat message is found', async () =>
        {
            try
            {
                await get('unknownMessage');
                expect.fail('Should have thrown an error');
            }
            catch (error)
            {
                expect(error.message).to.equal('No message with id \'unknownMessage\' found.');
            }
        });
    });

    describe('list', () =>
    {
        it('should list all chat messages for a given chat', async () =>
        {
            // Attempt to list all chat messages
            const messages = await list('chat1');

            // Check the chat messages
            expect(messages).to.have.length(3);
            expect(messages[0].id).to.equal('message1');
            expect(messages[0].author).to.equal('wrenchGirl70');
            expect(messages[0].content).to.equal('Hello, World!');

            expect(messages[1].id).to.equal('message2');
            expect(messages[1].author).to.equal('KwisatzHatTrick');
            expect(messages[1].content).to.equal('Greetings!');

            expect(messages[2].id).to.equal('message3');
            expect(messages[2].author).to.equal('KwisatzHatTrick');
            expect(messages[2].content).to.equal('Second Message.');
        });

        it('should list all chat messages for a given chat and starting from a given timestamp', async () =>
        {
            // Attempt to list all chat messages
            const messages = await list('chat1', (new Date('2621-01-02T00:00:00.000Z')).getTime());

            // Check the chat messages
            expect(messages).to.have.length(2);
            expect(messages[0].id).to.equal('message2');
            expect(messages[0].author).to.equal('KwisatzHatTrick');
            expect(messages[0].content).to.equal('Greetings!');
            expect(messages[1].id).to.equal('message3');
            expect(messages[1].author).to.equal('KwisatzHatTrick');
            expect(messages[1].content).to.equal('Second Message.');
        });

        it('should list all chat messages for a given chat and ending on a given timestamp', async () =>
        {
            // Attempt to list all chat messages
            const messages = await list('chat1', undefined, (new Date('2621-01-01T00:00:00.000Z')).getTime());

            // Check the chat messages
            expect(messages).to.have.length(1);
            expect(messages[0].id).to.equal('message1');
            expect(messages[0].author).to.equal('wrenchGirl70');
            expect(messages[0].content).to.equal('Hello, World!');
        });

        it('should list all chat messages for a given chat and starting from a given timestamp and ending on a given timestamp', async () =>
        {
            // Attempt to list all chat messages
            const messages = await list('chat1', (new Date('2621-01-01T00:00:00.000Z')).getTime(), (new Date('2621-01-02T00:00:00.000Z')).getTime());

            // Check the chat messages
            expect(messages).to.have.length(2);
            expect(messages[0].id).to.equal('message1');
            expect(messages[0].author).to.equal('wrenchGirl70');
            expect(messages[0].content).to.equal('Hello, World!');
            expect(messages[1].id).to.equal('message2');
            expect(messages[1].author).to.equal('KwisatzHatTrick');
            expect(messages[1].content).to.equal('Greetings!');
        });
    });

    describe('create', () =>
    {
        it('should create a chat message', async () =>
        {
            // Create a new chat message
            const message = {
                chatID: 'chat2',
                id: 'messagetest',
                content: 'Greetings!',
                author: 'AceHuntress62',
                createdAt: (new Date('2621-01-05T00:00:00.000Z')).getTime(),
            };

            // Attempt to create the chat message
            await create(message);

            // Get the chat message
            const createdMessage = await get('messagetest');

            // Check the chat message
            expect(createdMessage.id).to.equal('messagetest');
            expect(createdMessage.author).to.equal('AceHuntress62');
            expect(createdMessage.content).to.equal('Greetings!');
            expect(createdMessage.createdAt).to.equal(new Date('2621-01-05T00:00:00.000Z').getTime());
        });

        it('should throw an error when creating a chat message with the same id', async () =>
        {
            // Attempt to create a chat message that already exists
            try
            {
                await create({
                    chatID: 'chat2',
                    id: 'message4',
                    content: 'Greetings!',
                    author: 'AceHuntress62',
                    createdAt: (new Date('2621-01-05T00:00:00.000Z')).getTime(),
                });

                expect.fail('Should have thrown an error');
            }
            catch (error)
            {
                expect(error.message).to.equal('A chat message with id \'message4\' already exists.');
            }
        });

        it('should throw an error when creating a chat message for a chat that does not exist', async () =>
        {
            // Attempt to create a chat message for a chat that does not exist
            try
            {
                await create({
                    chatID: 'unknownChat',
                    id: 'message6',
                    content: 'Greetings!',
                    author: 'AceHuntress62',
                    createdAt: (new Date('2621-01-05T00:00:00.000Z')).getTime(),
                });

                expect.fail('Should have thrown an error');
            }
            catch (error)
            {
                expect(error.message).to.equal('No chat with id \'unknownChat\' found.');
            }
        });
    });

    describe('edit', () =>
    {
        it('should edit a chat message', async () =>
        {
            const editTS = new Date('2621-01-05T00:00:00.000Z').getTime();

            // Edit the chat message
            await edit('message1', 'Hello, Universe!', editTS);

            // Get the chat message
            const editedMessage = await get('message1');

            // Check the chat message
            expect(editedMessage.id).to.equal('message1');
            expect(editedMessage.author).to.equal('wrenchGirl70');
            expect(editedMessage.content).to.equal('Hello, Universe!');
            expect(editedMessage.editedAt).to.equal(editTS);
        });

        it('should throw an error when editing a chat message that does not exist', async () =>
        {
            try
            {
                await edit('unknownMessage', 'Hello, Universe!', new Date('2621-01-05T00:00:00.000Z').getTime());
                expect.fail('Should have thrown an error');
            }
            catch (error)
            {
                expect(error.message).to.equal('No message with id \'unknownMessage\' found.');
            }
        });
    });

    describe('update', () =>
    {
        it('should update a chat message', async () =>
        {
            // Update the chat message
            await update({
                id: 'message1',
                content: 'Hello, Universe!',
                editedAt: (new Date('2621-01-05T00:00:00.000Z')).getTime(),
            });

            // Get the chat message
            const updatedMessage = await get('message1');

            // Check the chat message
            expect(updatedMessage.id).to.equal('message1');
            expect(updatedMessage.author).to.equal('wrenchGirl70');
            expect(updatedMessage.content).to.equal('Hello, Universe!');
            expect(updatedMessage.editedAt).to.equal(new Date('2621-01-05T00:00:00.000Z').getTime());
        });

        it('should throw an error when updating a chat message that does not exist', async () =>
        {
            try
            {
                await update({
                    id: 'unknownMessage',
                    content: 'Hello, Universe!',
                    editedAt: (new Date('2621-01-05T00:00:00.000Z')).getTime(),
                });

                expect.fail('Should have thrown an error');
            }
            catch (error)
            {
                expect(error.message).to.equal('No message with id \'unknownMessage\' found.');
            }
        });
    });

    describe('remove', () =>
    {
        it('should remove a chat message', async () =>
        {
            // Remove `message1`
            await remove('message1');

            // Attempt to get `message1`
            try
            {
                await get('message1');
                expect.fail('Should have thrown an error');
            }
            catch (error)
            {
                expect(error.message).to.equal('No message with id \'message1\' found.');
            }
        });

        it('should not throw an error when the chat message does not exist', async () =>
        {
            // Remove `unknownMessage`
            await remove('unknownMessage');
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------

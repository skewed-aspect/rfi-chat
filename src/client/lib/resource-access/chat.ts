//----------------------------------------------------------------------------------------------------------------------
// Chat Resource Access
//----------------------------------------------------------------------------------------------------------------------

import axios from 'axios';

// Models
import { ChatExternal, ChatMessage } from '../../../common/models/chats.js';

//----------------------------------------------------------------------------------------------------------------------

class ChatResourceAccess
{
    async get(chatID) : Promise<ChatExternal | null>
    {
        // Get the chat from the server, handing a 404 error and returning null instead.
        try
        {
            const response = await axios.get(`/api/chat/${ chatID }`);
            return response.data;
        }
        catch (error)
        {
            if(error.response.status === 404)
            {
                return null;
            }

            throw error;
        }
    }

    async loadMessages(chatID : string) : Promise<ChatMessage[]>
    {
        // Get the messages from the server
        const response = await axios.get(`/api/message/${ chatID }`);
        return response.data;
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new ChatResourceAccess();

//----------------------------------------------------------------------------------------------------------------------

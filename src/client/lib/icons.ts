// ---------------------------------------------------------------------------------------------------------------------
// FontAwesome Icons
// ---------------------------------------------------------------------------------------------------------------------

// To add icons, simply use the export shorthand and export the icons under any name you'd like, as long as the
// versions from the different styles don't conflict. As long as the icon is exported from this file, it will be added
// to the library and will be made available for use.

// ---------------------------------------------------------------------------------------------------------------------

// Font Awesome Solid
export {
    faHome,
    faArrowRightToBracket,
} from '@fortawesome/sharp-solid-svg-icons';

// Font Awesome Regular
export {
    faComments,
    faQuestionCircle,
} from '@fortawesome/sharp-regular-svg-icons';

// Font Awesome Brands
export {
    faGitlab,
} from '@fortawesome/free-brands-svg-icons';

// ---------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
// MessageEngine
//----------------------------------------------------------------------------------------------------------------------

// Models
import { ChatMessage } from '../../../common/models/chats.js';

//----------------------------------------------------------------------------------------------------------------------

export interface GroupedMessages
{
    author : string;
    messages : ChatMessage[];
}

//----------------------------------------------------------------------------------------------------------------------

class MessageEngine
{
    /**
     * Group messages by user, so they can all be displayed together.
     *
     * @param messages - The messages to group.
     *
     * @returns The grouped messages.
     */
    groupMessages(messages : ChatMessage[]) : GroupedMessages[]
    {
        const groupedMessages : GroupedMessages[] = [];

        let currentGroup : ChatMessage[] = [];
        let currentGroupUser : string | null = null;

        for(const message of messages)
        {
            if(currentGroupUser === null || currentGroupUser === message.author)
            {
                currentGroup.push(message);
                currentGroupUser = message.author;
            }
            else
            {
                groupedMessages.push({ author: currentGroupUser, messages: currentGroup });
                currentGroup = [ message ];
                currentGroupUser = message.author;
            }
        }

        if(currentGroup.length > 0)
        {
            groupedMessages.push({ author: currentGroupUser, messages: currentGroup });
        }

        return groupedMessages;
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new MessageEngine();

//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
// ChatManager
//----------------------------------------------------------------------------------------------------------------------

// Resource Access
import chatRA from '../resource-access/chat.js';
import { ChatExternal, ChatMessage } from '../../../common/models/chats.js';

//----------------------------------------------------------------------------------------------------------------------

export interface ChatState
{
    chat : ChatExternal | null;
    messages : ChatMessage[];
}

//----------------------------------------------------------------------------------------------------------------------

class ChatManager
{
    async loadChat(chatID : string) : Promise<any>
    {
        const state : ChatState = { chat: null, messages: [] };

        // Load the chat from the chatID
        const chat = await chatRA.get(chatID);

        if(chat)
        {
            state.chat = chat;

            // Load the messages
            state.messages = await chatRA.loadMessages(chatID);
        }

        return state;
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new ChatManager();

//----------------------------------------------------------------------------------------------------------------------

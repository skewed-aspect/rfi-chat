// ---------------------------------------------------------------------------------------------------------------------
// Main App
// ---------------------------------------------------------------------------------------------------------------------

import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

// Components
import AppComponent from './app.vue';

// Pages
import AboutPage from './pages/aboutPage.vue';
import ChatPage from './pages/chatPage.vue';
import HomePage from './pages/homePage.vue';

// Site Style
import './scss/theme.scss';

// Site Icons
import * as icons from './lib/icons.js';

// ---------------------------------------------------------------------------------------------------------------------
// Vue Router
// ---------------------------------------------------------------------------------------------------------------------

const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', name: 'home', component: HomePage },
        { path: '/about', name: 'about', component: AboutPage },

        // Chat Program
        { path: '/chat/:chatID?', name: 'chat', component: ChatPage },

        // Fallback to home
        { path: '/:catchAll(.*)', redirect: { name: 'home' } },
    ],
});

// ---------------------------------------------------------------------------------------------------------------------
// Vue App
// ---------------------------------------------------------------------------------------------------------------------

const app = createApp(AppComponent);

// Add icons to the library
library.add(icons);

// Create the font awesome component
app.component('FaIcon', FontAwesomeIcon);

// Use the router we configured
app.use(router);

// ---------------------------------------------------------------------------------------------------------------------

// Initialization
async function init() : Promise<void>
{
    console.log('RFI Chat Generator initialized.');
}

// ---------------------------------------------------------------------------------------------------------------------

init()
    .then(() => app.mount('#main-app'))
    .catch((err) =>
    {
        console.error('Failed to initialize application:', err.stack);
    });

// ---------------------------------------------------------------------------------------------------------------------

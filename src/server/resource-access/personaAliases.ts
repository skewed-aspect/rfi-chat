// ---------------------------------------------------------------------------------------------------------------------
// Persona Aliases Resource Access
// ---------------------------------------------------------------------------------------------------------------------

import logging from '@strata-js/util-logging';

// Models
import { PersonaAlias, PersonaAliasDBRecord } from '../../common/models/personas.js';

// Utils
import { getDB } from '../utils/database.js';

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('personaAliasRA');

// ---------------------------------------------------------------------------------------------------------------------

function $toDBRecord(alias : PersonaAlias) : PersonaAliasDBRecord
{
    return {
        target: alias.target,
        owner: alias.owner,
        alias: alias.alias,
    };
}

function $fromDBRecord(record : PersonaAliasDBRecord) : PersonaAlias
{
    return {
        target: record.target,
        owner: record.owner,
        alias: record.alias,
    };
}

// ---------------------------------------------------------------------------------------------------------------------

export async function get(target : string, owner : string) : Promise<PersonaAlias | null>
{
    const db = await getDB();
    const aliases = await db<PersonaAliasDBRecord>('persona_alias')
        .select()
        .where({ target, owner });

    if(aliases.length > 1)
    {
        throw new Error('Multiple aliases found with the same id.');
    }
    else if(aliases.length === 0)
    {
        logger.debug(`No alias with username '${ target }' and owner '${ owner }' found.`);
        return null;
    }
    else
    {
        return $fromDBRecord(aliases[0]);
    }
}

export async function list(owner ?: string) : Promise<PersonaAlias[]>
{
    const db = await getDB();
    let aliases : PersonaAliasDBRecord[];

    if(owner)
    {
        aliases = await db<PersonaAliasDBRecord>('persona_alias')
            .select()
            .where({ owner });
    }
    else
    {
        aliases = await db<PersonaAliasDBRecord>('persona_alias')
            .select();
    }

    return aliases.map($fromDBRecord);
}

export async function create(alias : PersonaAlias) : Promise<void>
{
    const db = await getDB();
    const existingAlias = await db<PersonaAliasDBRecord>('persona_alias')
        .where({ target: alias.target, owner: alias.owner })
        .first();

    if(existingAlias)
    {
        throw new Error(`A persona alias with target '${ alias.target }' and owner '${ alias.owner }' already exists.`);
    }

    await db<PersonaAliasDBRecord>('persona_alias').insert($toDBRecord(alias));
}

export async function update(alias : PersonaAlias) : Promise<void>
{
    const db = await getDB();
    const existingAlias = await db<PersonaAliasDBRecord>('persona_alias')
        .where({ target: alias.target, owner: alias.owner })
        .first();

    if(!existingAlias)
    {
        throw new Error(`No alias with target '${ alias.target }' and owner '${ alias.owner }' found.`);
    }

    await db<PersonaAliasDBRecord>('persona_alias')
        .where({ target: alias.target, owner: alias.owner })
        .update($toDBRecord(alias));
}

export async function remove(target : string, owner : string) : Promise<void>
{
    const db = await getDB();
    await db<PersonaAliasDBRecord>('persona_alias')
        .where({ target, owner })
        .delete();
}

// ---------------------------------------------------------------------------------------------------------------------

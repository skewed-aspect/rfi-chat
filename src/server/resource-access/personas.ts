// ---------------------------------------------------------------------------------------------------------------------
// Persona Resource Access
// ---------------------------------------------------------------------------------------------------------------------

import logging from '@strata-js/util-logging';

// Models
import { Persona, PersonaDBRecord } from '../../common/models/personas.js';

// Utils
import { getDB } from '../utils/database.js';

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('personaRA');

// ---------------------------------------------------------------------------------------------------------------------

function $toDBRecord(persona : Persona) : PersonaDBRecord
{
    return {
        username: persona.username,
        full_name: persona.fullName,
        avatar: persona.avatar ? persona.avatar.buffer : null,
        avatar_mime: persona.avatar ? persona.avatar.mime : null,
        wiki_page: persona.wikiPage,
    };
}

function $fromDBRecord(record : PersonaDBRecord) : Persona
{
    return {
        username: record.username,
        fullName: record.full_name,
        avatar: record.avatar ? { buffer: record.avatar, mime: record.avatar_mime } : undefined,
        wikiPage: record.wiki_page ?? undefined,
    };
}

// ---------------------------------------------------------------------------------------------------------------------

export async function get(username : string) : Promise<Persona | null>
{
    const db = await getDB();
    const personas = await db<PersonaDBRecord>('persona')
        .select()
        .where({ username });

    if(personas.length > 1)
    {
        throw new Error('Multiple personas found with the same id.');
    }
    else if(personas.length === 0)
    {
        logger.debug(`No persona with username '${ username }' found.`);
        return null;
    }
    else
    {
        return $fromDBRecord(personas[0]);
    }
}

export async function list() : Promise<Persona[]>
{
    const db = await getDB();
    const personas = await db<PersonaDBRecord>('persona')
        .select();

    return personas.map($fromDBRecord);
}

export async function create(persona : Persona) : Promise<void>
{
    const db = await getDB();
    const existingPersona = await db<PersonaDBRecord>('persona')
        .where({ username: persona.username })
        .first();

    if(existingPersona)
    {
        throw new Error(`A persona with username '${ persona.username }' already exists.`);
    }

    await db<PersonaDBRecord>('persona').insert($toDBRecord(persona));
}

export async function update(persona : Persona) : Promise<void>
{
    const db = await getDB();
    const existingPersona = await db<PersonaDBRecord>('persona')
        .where({ username: persona.username })
        .first();

    if(!existingPersona)
    {
        throw new Error(`No persona with username '${ persona.username }' found.`);
    }

    await db<PersonaDBRecord>('persona')
        .where({ username: persona.username })
        .update($toDBRecord(persona));
}

export async function remove(username : string) : Promise<void>
{
    const db = await getDB();
    await db<PersonaDBRecord>('persona')
        .where({ username })
        .delete();
}

// ---------------------------------------------------------------------------------------------------------------------

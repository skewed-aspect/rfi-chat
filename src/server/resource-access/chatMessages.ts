// ---------------------------------------------------------------------------------------------------------------------
// Chat Messages Resource Access
// ---------------------------------------------------------------------------------------------------------------------

// Models
import { ChatMessage, ChatMessageDBRecord } from '../../common/models/chats.js';

// Utils
import { getDB } from '../utils/database.js';

// ---------------------------------------------------------------------------------------------------------------------

function $toDBRecord(message : ChatMessage) : ChatMessageDBRecord
{
    return {
        id: message.id,
        chat_id: message.chatID,
        author: message.author,
        content: message.content,
        created_at: message.createdAt,
        edited_at: message.editedAt,
    };
}

function $toDBRecordPartial(message : Partial<ChatMessage>) : Partial<ChatMessageDBRecord>
{
    // Build a partial record, matching what's in the message
    const record : Partial<ChatMessageDBRecord> = {};
    if(message.id)
    {
        record.id = message.id;
    }
    if(message.chatID)
    {
        record.chat_id = message.chatID;
    }
    if(message.author)
    {
        record.author = message.author;
    }
    if(message.content)
    {
        record.content = message.content;
    }
    if(message.createdAt)
    {
        record.created_at = message.createdAt;
    }
    if(message.editedAt)
    {
        record.edited_at = message.editedAt;
    }

    return record;
}

function $fromDBRecord(record : ChatMessageDBRecord) : ChatMessage
{
    return {
        id: record.id,
        chatID: record.chat_id,
        author: record.author,
        content: record.content,
        createdAt: record.created_at,
        editedAt: record.edited_at,
    };
}

// ---------------------------------------------------------------------------------------------------------------------

export async function get(id : string) : Promise<ChatMessage>
{
    const db = await getDB();
    const messages = await db<ChatMessageDBRecord>('chat_message')
        .select()
        .where({ id });

    if(messages.length > 1)
    {
        throw new Error('Multiple messages found with the same id.');
    }
    else if(messages.length === 0)
    {
        throw new Error(`No message with id '${ id }' found.`);
    }
    else
    {
        return $fromDBRecord(messages[0]);
    }
}

export async function list(chatID : string, startTimestamp ?: number, endTimestamp ?: number) : Promise<ChatMessage[]>
{
    const db = await getDB();
    let query = db<ChatMessageDBRecord>('chat_message')
        .select()
        .where({ chat_id: chatID });

    if(startTimestamp)
    {
        query = query.andWhere('created_at', '>=', startTimestamp);
    }
    if(endTimestamp)
    {
        query = query.andWhere('created_at', '<=', endTimestamp);
    }

    return (await query).map($fromDBRecord);
}

export async function create(message : ChatMessage) : Promise<void>
{
    const db = await getDB();

    // Check if the message already exists
    const existingMessage = await db<ChatMessageDBRecord>('chat_message')
        .where({ id: message.id })
        .first();
    if(existingMessage)
    {
        throw new Error(`A chat message with id '${ message.id }' already exists.`);
    }

    // Check if the chat exists
    const chatExists = await db('chat')
        .where({ id: message.chatID })
        .first();
    if(!chatExists)
    {
        throw new Error(`No chat with id '${ message.chatID }' found.`);
    }

    // Insert the new message
    await db<ChatMessageDBRecord>('chat_message').insert($toDBRecord(message));
}

// Note: This limits the ability to edit as if it were a real chat program
export async function edit(id : string, content : string, editTimestamp : number) : Promise<void>
{
    const db = await getDB();
    const existingMessage = await db<ChatMessageDBRecord>('chat_message')
        .where({ id })
        .first();

    if(!existingMessage)
    {
        throw new Error(`No message with id '${ id }' found.`);
    }

    await db<ChatMessageDBRecord>('chat_message')
        .where({ id })
        .update({ content, edited_at: editTimestamp });
}

// Note: this is for the making fake chat messages functionality. In a real chat program, it wouldn't exist.
export async function update(message : Partial<ChatMessage>) : Promise<void>
{
    const db = await getDB();
    const existingMessage = await db<ChatMessageDBRecord>('chat_message')
        .where({ id: message.id })
        .first();

    if(!existingMessage)
    {
        throw new Error(`No message with id '${ message.id }' found.`);
    }

    await db<ChatMessageDBRecord>('chat_message')
        .where({ id: message.id })
        .update($toDBRecordPartial(message));
}

export async function remove(id : string) : Promise<void>
{
    const db = await getDB();
    await db<ChatMessageDBRecord>('chat_message')
        .where({ id })
        .delete();
}

// ---------------------------------------------------------------------------------------------------------------------

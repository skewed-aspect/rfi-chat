// ---------------------------------------------------------------------------------------------------------------------
// Chat Aliases Resource Access
// ---------------------------------------------------------------------------------------------------------------------

import logging from '@strata-js/util-logging';

// Models
import { ChatAlias, ChatAliasDBRecord } from '../../common/models/chats.js';

// Utils
import { getDB } from '../utils/database.js';

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('chatAliasRA');

// ---------------------------------------------------------------------------------------------------------------------

function $toDBRecord(alias : ChatAlias) : ChatAliasDBRecord
{
    return {
        chat_id: alias.chatID,
        owner: alias.owner,
        alias: alias.alias,
        description: alias.description,
    };
}

function $fromDBRecord(record : ChatAliasDBRecord) : ChatAlias
{
    return {
        chatID: record.chat_id,
        owner: record.owner,
        alias: record.alias,
        description: record.description,
    };
}

// ---------------------------------------------------------------------------------------------------------------------

export async function get(chatID : string, owner : string) : Promise<ChatAlias | null>
{
    const db = await getDB();
    const aliases = await db<ChatAliasDBRecord>('chat_alias')
        .select()
        .where({ chat_id: chatID, owner });

    if(aliases.length > 1)
    {
        throw new Error('Multiple aliases found with the same id.');
    }
    else if(aliases.length === 0)
    {
        logger.debug(`No chat alias with id '${ chatID }' and owner '${ owner }' found.`);
        return null;
    }
    else
    {
        return $fromDBRecord(aliases[0]);
    }
}

export async function list(owner ?: string) : Promise<ChatAlias[]>
{
    const db = await getDB();
    let aliases : ChatAliasDBRecord[];

    console.log('hmm?', owner);

    if(owner)
    {
        aliases = await db<ChatAliasDBRecord>('chat_alias')
            .select()
            .where({ owner });
    }
    else
    {
        aliases = await db<ChatAliasDBRecord>('chat_alias')
            .select();
    }

    console.log('foo:', aliases);

    return aliases.map($fromDBRecord);
}

export async function create(alias : ChatAlias) : Promise<void>
{
    const db = await getDB();
    const existingAlias = await db<ChatAliasDBRecord>('chat_alias')
        .where({ chat_id: alias.chatID, owner: alias.owner })
        .first();

    if(existingAlias)
    {
        throw new Error(`A chat alias with chatID '${ alias.chatID }', owner '${ alias.owner }' already exists.`);
    }

    await db<ChatAliasDBRecord>('chat_alias').insert($toDBRecord(alias));
}

export async function update(alias : ChatAlias) : Promise<void>
{
    const db = await getDB();
    const existingAlias = await db<ChatAliasDBRecord>('chat_alias')
        .where({ chat_id: alias.chatID, owner: alias.owner })
        .first();

    if(!existingAlias)
    {
        throw new Error(`Chat alias with chatID '${ alias.chatID }', owner '${ alias.owner }' does not exist.`);
    }

    await db<ChatAliasDBRecord>('chat_alias')
        .where({ chat_id: alias.chatID, owner: alias.owner })
        .update($toDBRecord(alias));
}

export async function remove(chatID : string, owner : string) : Promise<void>
{
    const db = await getDB();
    await db<ChatAliasDBRecord>('chat_alias')
        .where({ chat_id: chatID, owner })
        .delete();
}

// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// Chat Resource Access
// ---------------------------------------------------------------------------------------------------------------------

import logging from '@strata-js/util-logging';

// Models
import { Chat, ChatDBRecord, ChatMemberDBRecord } from '../../common/models/chats.js';

// Utils
import { getDB } from '../utils/database.js';

// ---------------------------------------------------------------------------------------------------------------------

function $toDBRecord(chat : Chat) : ChatDBRecord
{
    return {
        id: chat.id,
        name: chat.name,
        description: chat.description,
        avatar: chat.avatar ? chat.avatar.buffer : null,
        avatar_mime: chat.avatar ? chat.avatar.mime : null,
    };
}

async function $fromDBRecord(record : ChatDBRecord, members ?: ChatMemberDBRecord[]) : Promise<Chat>
{
    const chatMembers = members?.map((member) =>
    {
        return member.persona;
    });

    return {
        id: record.id,
        name: record.name,
        description: record.description,
        avatar: record.avatar ? { buffer: record.avatar, mime: record.avatar_mime } : undefined,
        members: members ? chatMembers : [],
    };
}

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('chatRA');

// ---------------------------------------------------------------------------------------------------------------------

export async function get(id : string) : Promise<Chat | null>
{
    const db = await getDB();
    const chats = await db<ChatDBRecord>('chat')
        .select()
        .where({ id });

    if(chats.length > 1)
    {
        throw new Error('Multiple chats found with the same id.');
    }
    else if(chats.length === 0)
    {
        logger.debug(`No chat with id '${ id }' found.`);
        return null;
    }
    else
    {
        // Get the list of chat members
        const members = await db<ChatMemberDBRecord>('chat_member')
            .select()
            .where({ chat_id: id })
            .orderBy('id');

        return $fromDBRecord(chats[0], members);
    }
}

export async function list(forUser ?: string) : Promise<Chat[]>
{
    const db = await getDB();
    let chatRecords : ChatDBRecord[];

    if(forUser)
    {
        chatRecords = await db<ChatDBRecord>('chat')
            .select()
            .whereRaw('id IN (SELECT chat_id FROM chat_member WHERE persona = ?)', [ forUser ]);
    }
    else
    {
        chatRecords = await db<ChatDBRecord>('chat')
            .select();
    }

    return Promise.all(chatRecords.map(async (record) =>
    {
        // Get the list of chat members
        const members = await db<ChatMemberDBRecord>('chat_member')
            .select()
            .where({ chat_id: record.id })
            .orderBy('id');

        return $fromDBRecord(record, members);
    }));
}

export async function create(chat : Chat) : Promise<void>
{
    const db = await getDB();
    const existingChat = await db<ChatDBRecord>('chat')
        .where({ id: chat.id })
        .first();

    if(existingChat)
    {
        throw new Error(`Chat with id '${ chat.id }' already exists.`);
    }

    await db<ChatDBRecord>('chat').insert($toDBRecord(chat));

    await Promise.all(chat.members.map(async (member) =>
    {
        await db<ChatMemberDBRecord>('chat_member')
            .insert({ chat_id: chat.id, persona: member });
    }));
}

export async function addMember(chatID : string, member : string) : Promise<void>
{
    const db = await getDB();
    const existingMember = await db<ChatMemberDBRecord>('chat_member')
        .where({ chat_id: chatID, persona: member })
        .first();

    if(existingMember)
    {
        throw new Error(`Member '${ member }' is already in chat '${ chatID }'.`);
    }

    await db<ChatMemberDBRecord>('chat_member')
        .insert({ chat_id: chatID, persona: member });
}

export async function removeMember(chatID : string, member : string) : Promise<void>
{
    const db = await getDB();
    await db<ChatMemberDBRecord>('chat_member')
        .where({ chat_id: chatID, persona: member })
        .delete();
}

export async function update(chat : Partial<Omit<Chat, 'members'>>) : Promise<void>
{
    const db = await getDB();
    const existingChat = await db<ChatDBRecord>('chat')
        .where({ id: chat.id })
        .first();

    if(!existingChat)
    {
        throw new Error(`Chat with id '${ chat.id }' does not exist.`);
    }

    const updateObject : Partial<ChatDBRecord> = {};

    // Check and add properties to the update object if they exist
    if(chat.name !== undefined)
    {
        updateObject.name = chat.name;
    }
    if(chat.description !== undefined)
    {
        updateObject.description = chat.description;
    }
    if(chat.avatar !== undefined)
    {
        updateObject.avatar = chat.avatar.buffer;
        updateObject.avatar_mime = chat.avatar.mime;
    }

    // Perform the update only if there's something to update
    if(Object.keys(updateObject).length > 0)
    {
        await db<ChatDBRecord>('chat')
            .where({ id: chat.id })
            .update(updateObject);
    }
}

export async function remove(id : string) : Promise<void>
{
    const db = await getDB();
    await db<ChatDBRecord>('chat')
        .where({ id })
        .delete();
}

// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// Add Base Personas
// ---------------------------------------------------------------------------------------------------------------------

import { resolve } from 'node:path';
import { Knex } from 'knex';
import { PersonaDBRecord } from '../../../common/models/personas.js';
import { getAvatarFromPath } from '../../utils/avatars.js';

// ---------------------------------------------------------------------------------------------------------------------

async function buildPersonaSeeds() : Promise<PersonaDBRecord[]>
{
    const wrenchAvatar = await getAvatarFromPath(resolve(import.meta.dirname, '../../../common/avatars/wrenchGirl70.png'));
    const kwisatzAvatar = await getAvatarFromPath(resolve(import.meta.dirname, '../../../common/avatars/KwisatzHatTrick.png'));
    const aceAvatar = await getAvatarFromPath(resolve(import.meta.dirname, '../../../common/avatars/AceHuntress62.png'));

    return [
        {
            username: 'system',
            full_name: 'System',
        },
        {
            username: 'wrenchGirl70',
            full_name: 'Whitney Antares',
            avatar: wrenchAvatar.buffer,
            avatar_mime: wrenchAvatar.mime,
            wiki_page: 'https://rfiuniverse.com/characters/whitney-antares',
        },
        {
            username: 'KwisatzHatTrick',
            full_name: 'David McKenzie',
            avatar: kwisatzAvatar.buffer,
            avatar_mime: kwisatzAvatar.mime,
            wiki_page: 'https://rfiuniverse.com/characters/david-mckenzie',
        },
        {
            username: 'AceHuntress62',
            full_name: 'Glyse McKenzie',
            avatar: aceAvatar.buffer,
            avatar_mime: aceAvatar.mime,
            wiki_page: 'https://rfiuniverse.com/characters/glyse-mckenzie',
        },
    ] satisfies PersonaDBRecord[];
}

export async function seed(knex : Knex) : Promise<void>
{
    // Build the personas list
    const personas = await buildPersonaSeeds();

    // Build a list of usernames
    const usernames = personas.map((pers) => pers.username);

    // Deletes ALL entries in the list of persona seeds
    await knex('persona')
        .del()
        .whereIn('username', usernames);

    // Inserts seed entries
    await knex('persona').insert(personas);
}

// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// Initial DB setup
// ---------------------------------------------------------------------------------------------------------------------

import type { Knex } from 'knex';

// ---------------------------------------------------------------------------------------------------------------------

export async function up(knex : Knex) : Promise<void>
{
    await knex.schema.createTable('persona', (table) =>
    {
        table.string('username').primary();
        table.string('full_name').notNullable();
        table.binary('avatar').nullable();
        table.string('avatar_mime').nullable();
        table.string('wiki_page').nullable();
    });

    await knex.schema.createTable('persona_alias', (table) =>
    {
        table.string('target').notNullable();
        table.string('owner').notNullable();
        table.string('alias').notNullable();

        table.foreign('target').references('persona.username')
            .onDelete('CASCADE');
        table.foreign('owner').references('persona.username')
            .onDelete('CASCADE');

        table.primary([ 'target', 'owner' ]);
    });

    await knex.schema.createTable('chat', (table) =>
    {
        table.string('id').primary();
        table.string('name').nullable();
        table.string('description').nullable();
        table.string('avatar').nullable();
        table.string('avatar_mime').nullable();
    });

    await knex.schema.createTable('chat_alias', (table) =>
    {
        table.string('chat_id').notNullable();
        table.string('owner').notNullable();
        table.string('alias').notNullable();
        table.string('description').nullable();

        table.foreign('chat_id').references('chat.id')
            .onDelete('CASCADE');
        table.foreign('owner').references('persona.username')
            .onDelete('CASCADE');

        table.primary([ 'chat_id', 'owner' ]);
    });

    await knex.schema.createTable('chat_member', (table) =>
    {
        table.increments('id').primary();
        table.string('chat_id').notNullable();
        table.string('persona').notNullable();

        table.foreign('chat_id').references('chat.id')
            .onDelete('CASCADE');
        table.foreign('persona').references('persona.username')
            .onDelete('CASCADE');

        table.index('chat_id');
        table.unique([ 'chat_id', 'persona' ]);
    });

    await knex.schema.createTable('chat_message', (table) =>
    {
        table.string('id').primary();
        table.string('chat_id').notNullable();
        table.string('author').notNullable();
        table.string('content').notNullable();
        table.timestamp('created_at')
            .nullable()
            .defaultTo(knex.fn.now());
        table.timestamp('edited_at').nullable();

        table.foreign('chat_id').references('chat.id')
            .onDelete('CASCADE');
        table.foreign('author').references('persona.username')
            .onDelete('CASCADE');
    });
}

// ---------------------------------------------------------------------------------------------------------------------

export async function down(knex : Knex) : Promise<void>
{
    await knex.schema.dropTable('chat_message');
    await knex.schema.dropTable('chat_member');
    await knex.schema.dropTable('chat_alias');
    await knex.schema.dropTable('chat');
    await knex.schema.dropTable('persona_alias');
    await knex.schema.dropTable('persona');
}

// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// Avatar Utils
// ---------------------------------------------------------------------------------------------------------------------

import { readFile } from 'node:fs/promises';

// Interfaces
import { ImageBuffer } from '../../common/interfaces/images.js';

// ---------------------------------------------------------------------------------------------------------------------

export async function getAvatarFromPath(path : string) : Promise<ImageBuffer>
{
    const ft = await import('file-type');
    const buffer = await readFile(path);
    const mime = await ft.fileTypeFromBuffer(buffer);

    if(mime === undefined)
    {
        throw new Error('Could not determine file type.');
    }

    return { mime: mime.mime, buffer };
}

export function imageBufferToDataURI(avatar : ImageBuffer) : string
{
    return `data:${ avatar.mime };base64,${ avatar.buffer.toString('base64') }`;
}

export function dataURIToImageBuffer(dataURI : string) : ImageBuffer
{
    const [ , mime, buffer ] = dataURI.match(/^data:(.*);base64,(.*)$/);

    return { mime, buffer: Buffer.from(buffer, 'base64') };
}

// ---------------------------------------------------------------------------------------------------------------------

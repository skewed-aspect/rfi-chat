//----------------------------------------------------------------------------------------------------------------------
// ChatManager
//----------------------------------------------------------------------------------------------------------------------

// Engines
import chatEngine from '../engines/chat.js';
import personaEngine from '../engines/persona.js';

// Models
import { Chat, ChatAlias, ChatExternal, PartialChat } from '../../common/models/chats.js';
import { Persona, PersonaExternal } from '../../common/models/personas.js';

// Resource Access
import * as chatRA from '../resource-access/chat.js';
import * as chatAliasRA from '../resource-access/chatAliases.js';
import * as personaRA from '../resource-access/personas.js';

//----------------------------------------------------------------------------------------------------------------------

class ChatManager
{
    // -----------------------------------------------------------------------------------------------------------------
    // Public Methods
    // -----------------------------------------------------------------------------------------------------------------

    public async get(id : string) : Promise<ChatExternal | null>
    {
        const chat = await chatRA.get(id);
        if(!chat)
        {
            return null;
        }

        // Format the chat for external use
        const externalChat = chatEngine.formatChatForExternal(chat);

        // Get the list of chat members as Persona objects
        const members = await Promise.all(chat.members.map(async (member) =>
        {
            const persona = await personaRA.get(member);
            if(persona)
            {
                return personaEngine.formatPersonaForExternal(persona);
            }
            else
            {
                return null;
            }
        }));

        return {
            ...externalChat,
            members,
        };
    }

    public async getAlias(id : string, owner : string) : Promise<ChatAlias | null>
    {
        return chatAliasRA.get(id, owner);
    }

    public async list(forUser ?: string) : Promise<ChatExternal[]>
    {
        const personaCache = new Map<string, PersonaExternal>();
        const chatList = await chatRA.list(forUser);

        return Promise.all(chatList.map(async (chat) =>
        {
            // Format the chat for external use
            const externalChat = chatEngine.formatChatForExternal(chat);

            // Get the list of chat members as Persona objects
            const members = await Promise.all(chat.members.map(async (member) =>
            {
                if(personaCache.has(member))
                {
                    return personaCache.get(member);
                }
                else
                {
                    const persona = await personaRA.get(member);
                    if(persona)
                    {
                        const externalPersona = personaEngine.formatPersonaForExternal(persona);
                        personaCache.set(member, externalPersona);
                        return externalPersona;
                    }
                    else
                    {
                        return null;
                    }
                }
            }));

            return {
                ...externalChat,
                members,
            };
        }));
    }

    public async listAliases(owner ?: string) : Promise<ChatAlias[]>
    {
        return chatAliasRA.list(owner);
    }

    public async create(chat : Chat) : Promise<void>
    {
        await chatRA.create(chat);
    }

    public async createAlias(alias : ChatAlias) : Promise<void>
    {
        await chatAliasRA.create(alias);
    }

    public async update(chatUpdate : PartialChat) : Promise<void>
    {
        await chatRA.update(chatUpdate);
    }

    public async updateAlias(alias : ChatAlias) : Promise<void>
    {
        await chatAliasRA.update(alias);
    }

    public async remove(id : string) : Promise<void>
    {
        await chatRA.remove(id);
    }

    public async removeAlias(owner : string, chatID : string) : Promise<void>
    {
        await chatAliasRA.remove(owner, chatID);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new ChatManager();

//----------------------------------------------------------------------------------------------------------------------

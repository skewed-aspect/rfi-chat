// ---------------------------------------------------------------------------------------------------------------------
// Persona Manager
// ---------------------------------------------------------------------------------------------------------------------

// Models
import { PartialPersona, Persona, PersonaAlias } from '../../common/models/personas.js';

// Engines
import personaEngine from '../engines/persona.js';

// Resource Access
import * as personaRA from '../resource-access/personas.js';
import * as personaAliasRA from '../resource-access/personaAliases.js';

// ---------------------------------------------------------------------------------------------------------------------

export class PersonaManager
{
    // -----------------------------------------------------------------------------------------------------------------
    // Public Methods
    // -----------------------------------------------------------------------------------------------------------------

    public async get(username : string) : Promise<Persona | null>
    {
        return personaRA.get(username);
    }

    public async getAlias(username : string, owner : string) : Promise<PersonaAlias | null>
    {
        return personaAliasRA.get(owner, username);
    }

    public async list() : Promise<Persona[]>
    {
        return personaRA.list();
    }

    public async listAliases(username ?: string) : Promise<PersonaAlias[]>
    {
        return personaAliasRA.list(username);
    }

    public async create(persona : Persona) : Promise<void>
    {
        await personaRA.create(persona);
    }

    public async createAlias(owner : string, target: string, alias : string) : Promise<void>
    {
        const newAliasObj : PersonaAlias = {
            owner,
            alias,
            target,
        };

        await personaAliasRA.create(newAliasObj);
    }

    public async update(personaUpdate : PartialPersona) : Promise<void>
    {
        // Get the current Persona
        const persona = await personaRA.get(personaUpdate.username);

        // Get the engine to build us a valid update
        const updatedPersona = await personaEngine.buildPersonaUpdate(personaUpdate, persona);

        // Update the Persona
        await personaRA.update(updatedPersona);
    }

    public async updateAlias(owner : string, target : string, alias : string) : Promise<void>
    {
        // Get the current Alias
        const personaAlias = await personaAliasRA.get(target, owner);
        if(!personaAlias)
        {
            await this.createAlias(owner, target, alias);
        }
        else
        {
            const aliasObj : PersonaAlias = {
                owner,
                alias,
                target,
            };

            // Update the Alias
            await personaAliasRA.update(aliasObj);
        }
    }

    public async delete(username : string) : Promise<void>
    {
        await personaRA.remove(username);
    }

    public async deleteAlias(username : string, target : string) : Promise<void>
    {
        await personaAliasRA.remove(username, target);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new PersonaManager();

// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// Chat Route
// ---------------------------------------------------------------------------------------------------------------------

import { Router } from 'express';

// Engines
import chatEngine from '../engines/chat.js';

// Managers
import chatMan from '../managers/chat.js';

// ---------------------------------------------------------------------------------------------------------------------

const router = Router();

// ---------------------------------------------------------------------------------------------------------------------

router.get('/', async (req, resp) =>
{
    // TODO: Parse this with something that does validation
    const persona = req.query.persona as string | undefined;

    // List all chats, or filter it to a persona
    const chats = await chatMan.list(persona);

    // Return the list of chats
    resp.json(chats);
});

router.get('/:chatID', async (req, resp) =>
{
    const chatID = req.params.chatID;

    // List a specific chat
    const chat = await chatMan.get(chatID);

    // If we got a chat, return it, otherwise return a 404
    if(chat)
    {
        // Return a specific chat
        resp.json(chat);
    }
    else
    {
        resp
            .status(404)
            .json({
                message: `Chat ${ chatID } not found`,
                chatID,
            });
    }
});

router.get('/:chatID/alias/:owner', async (req, resp) =>
{
    const chatID = req.params.chatID;
    const owner = req.params.owner;

    const chat = await chatMan.get(chatID);

    // If we got a chat, get the alias, otherwise return a 404
    if(chat)
    {
        const aliases = await chatMan.getAlias(chatID, owner);
        resp.json(aliases);
    }
    else
    {
        resp
            .status(404)
            .json({
                message: `Chat ${ chatID } not found, not getting alias for unknown chat.`,
                chatID,
            });
    }
});

router.post('/', async (req, resp) =>
{
    const chat = req.body;

    // Validate the chat
    const validationErrors = await chatEngine.validateChat(chat);

    // If there are validation errors, return a 400
    if(validationErrors.length > 0)
    {
        resp
            .status(400)
            .json({
                message: 'Chat validation failed.',
                validationErrors,
            });
    }
    else
    {
        // Create a new chat
        await chatMan.create(chat);
        resp.status(201).end();
    }
});

router.post('/:chatID/alias/:owner', async (req, resp) =>
{
    const chatID = req.params.chatID;
    const owner = req.params.owner;

    const alias = req.body.alias;
    const description = req.body.description;

    // If the alias is not a string, return a 400
    if(!alias || typeof alias !== 'string')
    {
        resp
            .status(400)
            .json({
                message: 'Alias must be a string.',
                alias,
            });
    }
    else
    {
        // Create a new chat alias
        await chatMan.createAlias({
            chatID,
            owner,
            alias,
            description,
        });
        resp.status(201).end();
    }
});

router.patch('/:chatID', async (req, resp) =>
{
    const chatID = req.params.chatID;

    const chat = {
        ...req.body,
        id: chatID,
    };

    // Validate the chat
    const validationErrors = await chatEngine.validateChat(chat);

    // If there are validation errors, return a 400
    if(validationErrors.length > 0)
    {
        resp
            .status(400)
            .json({
                message: 'Chat validation failed.',
                validationErrors,
            });
    }
    else
    {
        // Update a chat
        await chatMan.update(chat);
        resp.status(204).end();
    }
});

router.patch('/:chatID/alias/:owner', async (req, resp) =>
{
    const chatID = req.params.chatID;
    const owner = req.params.owner;

    const alias = req.body.alias;
    const description = req.body.description;

    // Get the existing alias, if there is one
    const existingAlias = await chatMan.getAlias(chatID, owner);

    // If the alias is not a string, return a 400
    if(!alias || typeof alias !== 'string')
    {
        resp
            .status(400)
            .json({
                message: 'Alias must be a string.',
                alias,
            });
        return;
    }

    // If there is no existing alias, return a 404
    if(!existingAlias)
    {
        resp
            .status(404)
            .json({
                message: 'Alias not found.',
                chatID,
                owner,
            });
    }
    else
    {
        // Update a chat alias
        await chatMan.updateAlias({
            chatID,
            owner,
            alias,
            description,
        });
        resp.status(204).end();
    }
});

router.delete('/:chatID', async (req, resp) =>
{
    const chatID = req.params.chatID;

    // Delete a chat
    await chatMan.remove(chatID);
    resp.status(204).end();
});

router.delete('/:chatID/alias/:owner', async (req, resp) =>
{
    const chatID = req.params.chatID;
    const owner = req.params.owner;

    // Delete a chat alias
    await chatMan.removeAlias(chatID, owner);
    resp.status(204).end();
});

// ---------------------------------------------------------------------------------------------------------------------

export default router;

// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// Persona Route
// ---------------------------------------------------------------------------------------------------------------------

import { Router } from 'express';

// Managers
import personaMan from '../managers/persona.js';

// Engines
import personaEngine from '../engines/persona.js';

// ---------------------------------------------------------------------------------------------------------------------

const router = Router();

// ---------------------------------------------------------------------------------------------------------------------

router.get('/', async (_req, resp) =>
{
    // List all personas
    const personas = await personaMan.list();

    // Return the list of personas
    resp.json(personas);
});

router.get('/:username', async (req, resp) =>
{
    const username = req.params.username;

    // Get a specific persona
    const persona = await personaMan.get(username);

    // If we got a persona, return it, otherwise return a 404
    if(persona)
    {
        // Return a specific persona
        resp.json(persona);
    }
    else
    {
        resp
            .status(404)
            .json({
                message: `Persona ${ username } not found`,
                username,
            });
    }
});

router.get('/:owner/alias', async (req, resp) =>
{
    const owner = req.params.owner;

    // Get the persona
    const persona = await personaMan.get(owner);

    // If we got a persona, get the aliases, otherwise return a 404
    if(persona)
    {
        // Get the aliases
        const aliases = await personaMan.listAliases(owner);

        // Return the aliases
        resp.json(aliases);
    }
    else
    {
        resp
            .status(404)
            .json({
                message: `Persona ${ owner } not found, not listing aliases for unknown persona.`,
                persona: owner,
            });
    }
});

router.get('/:owner/alias/:target', async (req, resp) =>
{
    const owner = req.params.owner;
    const target = req.params.target;

    // Get the personas
    const ownerPersona = await personaMan.get(owner);
    const targetPersona = await personaMan.get(target);

    // If we got both personas, get the alias, otherwise return a 404
    if(ownerPersona && targetPersona)
    {
        // Get the alias
        const alias = await personaMan.getAlias(owner, target);

        // If we got an alias, return it, otherwise return a 404
        if(alias)
        {
            // Return the alias
            resp.json(alias);
        }
        else
        {
            resp
                .status(404)
                .json({
                    message: `Alias for ${ target } not found for owner ${ owner }`,
                    owner,
                    target,
                });
        }
    }
    else
    {
        let message = '';
        if(!ownerPersona)
        {
            message = `Persona ${ owner } not found, not listing alias for unknown persona.`;
        }

        if(!targetPersona)
        {
            message = `Persona ${ target } not found, not listing alias for unknown persona.`;
        }

        if(!ownerPersona && !targetPersona)
        {
            message = `Personas ${ owner } and ${ target } not found, not listing alias for unknown persona.`;
        }

        resp
            .status(404)
            .json({
                message,
                owner,
                target,
            });
    }
});

router.post('/', async (req, resp) =>
{
    // Create a new persona
    const persona = req.body;

    const validationErrors = await personaEngine.validatePersona(persona);
    if(validationErrors.length > 0)
    {
        resp
            .status(400)
            .json({
                message: 'Persona validation failed.',
                validationErrors,
            });
    }
    else
    {
        await personaMan.create(persona);
        resp.status(201).end();
    }
});

router.post('/:owner/alias', async (req, resp) =>
{
    const owner : string = req.params.owner;
    const alias : string = req.body.alias;
    const target : string = req.body.target;

    if(!alias)
    {
        resp
            .status(400)
            .json({
                message: 'Alias must be specified.',
                owner,
                target,
            });
        return;
    }

    if(!target)
    {
        resp
            .status(400)
            .json({
                message: 'Target must be specified.',
                owner,
                alias,
            });
        return;
    }

    const ownerPersona = await personaMan.get(owner);
    const targetPersona = await personaMan.get(target);

    if(ownerPersona && targetPersona)
    {
        await personaMan.createAlias(owner, target, alias);
        resp.status(201).end();
    }
    else
    {
        let message = '';
        if(!ownerPersona)
        {
            message = `Persona ${ owner } not found, not creating alias for unknown persona.`;
        }

        if(!targetPersona)
        {
            message = `Persona ${ target } not found, not creating alias for unknown persona.`;
        }

        if(!ownerPersona && !targetPersona)
        {
            message = `Personas ${ owner } and ${ target } not found, not creating alias for unknown persona.`;
        }

        resp
            .status(404)
            .json({
                message,
                owner,
                target,
            });
    }
});

router.patch('/:username', async (req, resp) =>
{
    const username = req.params.username;

    // Update a persona
    const persona = {
        ...req.body,
        username,
    };

    const validationErrors = await personaEngine.validatePersona(persona);
    if(validationErrors.length > 0)
    {
        resp
            .status(400)
            .json({
                message: 'Persona validation failed.',
                validationErrors,
            });
    }
    else
    {
        await personaMan.update(persona);
        resp.status(204).end();
    }
});

router.patch('/:owner/alias/:target', async (req, resp) =>
{
    const owner = req.params.owner;
    const target = req.params.target;
    const alias = req.body.alias;

    // Update an alias
    const ownerPersona = await personaMan.get(owner);
    const targetPersona = await personaMan.get(target);

    if(ownerPersona && targetPersona)
    {
        await personaMan.updateAlias(owner, target, alias);
        resp.status(204).end();
    }
    else
    {
        let message = '';
        if(!ownerPersona)
        {
            message = `Persona ${ owner } not found, not updating alias for unknown persona.`;
        }

        if(!targetPersona)
        {
            message = `Persona ${ target } not found, not updating alias for unknown persona.`;
        }

        if(!ownerPersona && !targetPersona)
        {
            message = `Personas ${ owner } and ${ target } not found, not updating alias for unknown persona.`;
        }

        resp
            .status(404)
            .json({
                message,
                owner,
                target,
            });
    }
});

router.delete('/:username', async (req, resp) =>
{
    const username = req.params.username;

    // Delete a persona
    await personaMan.delete(username);
    resp.status(204).end();
});

router.delete('/:owner/alias/:target', async (req, resp) =>
{
    const owner = req.params.owner;
    const target = req.params.target;

    // Delete an alias
    await personaMan.deleteAlias(owner, target);
    resp.status(204).end();
});

// ---------------------------------------------------------------------------------------------------------------------

export default router;

// ---------------------------------------------------------------------------------------------------------------------

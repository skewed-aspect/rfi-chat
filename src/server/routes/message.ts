// ---------------------------------------------------------------------------------------------------------------------
// Version Route
// ---------------------------------------------------------------------------------------------------------------------

import { Router } from 'express';

// Managers
import chatMan from '../managers/chat.js';
import messageMan from '../managers/messages.js';

// ---------------------------------------------------------------------------------------------------------------------

const router = Router();

// ---------------------------------------------------------------------------------------------------------------------

router.get('/:chatID', async (req, resp) =>
{
    const chatID = req.params.chatID;

    // List a specific chat
    const chat = await chatMan.get(chatID);

    // If we got a chat, return it, otherwise return a 404
    if(chat)
    {
        // List all messages for the chat
        const messages = await messageMan.list(chatID);
        resp.json(messages);
    }
    else
    {
        resp
            .status(404)
            .json({
                message: `Chat ${ chatID } not found`,
                chatID,
            });
    }
});

router.get('/:chatID/:messageID', async (req, resp) =>
{
    const chatID = req.params.chatID;
    const messageID = req.params.messageID;

    // List a specific chat
    const message = await messageMan.get(messageID);

    // If we got a chat, return it, otherwise return a 404
    if(message)
    {
        if(message.chatID !== chatID)
        {
            resp
                .status(422)
                .json({
                    message: `Message ${ messageID } is not for chat ${ chatID }`,
                    messageID,
                    chatID,
                });
        }
        else
        {
            // Return a specific chat
            resp.json(message);
        }
    }
    else
    {
        resp
            .status(404)
            .json({
                message: `Message ${ messageID } not found`,
                messageID,
            });
    }
});

router.post('/:chatID', async (req, resp) =>
{
    const chatID = req.params.chatID;
    const message = req.body;

    const chatMessage = {
        ...message,
        id: null,
        chatID,
    };

    // Create a message
    await messageMan.create(chatMessage);
    resp.status(201).end();
});

router.patch('/:chatID/:messageID', async (req, resp) =>
{
    const chatID = req.params.chatID;
    const messageID = req.params.messageID;
    const message = req.body;

    const chatMessage = {
        ...message,
        chatID,
        id: messageID,
    };

    // TODO: If this were a real chat message, this should be `messageMan.edit(chatMessage);`.

    // Update a message
    await messageMan.update(chatMessage);
    resp.status(204).end();
});

router.delete('/:chatID/:messageID', async (req, resp) =>
{
    const chatID = req.params.chatID;
    const messageID = req.params.messageID;

    // Look up the message
    const message = await messageMan.get(messageID);

    // If the message exists, make sure it's for the right chat
    if(message && message.chatID !== chatID)
    {
        resp
            .status(422)
            .json({
                message: `Message ${ messageID } is not for chat ${ chatID }`,
                messageID,
                chatID,
            });
    }
    else
    {
        // Delete a message
        await messageMan.remove(messageID);
        resp.status(204).end();
    }
});

// ---------------------------------------------------------------------------------------------------------------------

export default router;

// ---------------------------------------------------------------------------------------------------------------------

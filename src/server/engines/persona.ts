//----------------------------------------------------------------------------------------------------------------------
// PersonaEngine
//----------------------------------------------------------------------------------------------------------------------

// Models
import { Persona, PersonaExternal } from '../../common/models/personas.js';
import { ValidationError } from '../../common/interfaces/validation.js';

//----------------------------------------------------------------------------------------------------------------------

class PersonaEngine
{
    async validatePersona(persona : Persona) : Promise<ValidationError[]>
    {
        const validationErrors : ValidationError[] = [];

        if(!persona.username)
        {
            validationErrors.push({ message: 'Persona must have a username.', field: 'username' });
        }

        if(!persona.fullName)
        {
            validationErrors.push({ message: 'Persona must have a full name.', field: 'fullName' });
        }

        if(persona.avatar && !persona.avatar.buffer)
        {
            validationErrors.push({ message: 'If specified, persona avatar must have a buffer.', field: 'avatar' });
        }

        if(persona.avatar && !persona.avatar.mime)
        {
            validationErrors.push({ message: 'If specified, persona avatar must have a mime type.', field: 'avatar' });
        }

        return validationErrors;
    }

    async buildPersonaUpdate(update : Partial<Persona>, persona : Persona) : Promise<Persona>
    {
        return {
            ...persona,
            ...update,
        };
    }

    formatPersonaForExternal(persona : Persona) : PersonaExternal
    {
        return {
            username: persona.username,
            fullName: persona.fullName,
            avatar: persona.avatar ? `data:${ persona.avatar.mime };base64,${ persona.avatar.buffer.toString('base64') }` : undefined,
            wikiPage: persona.wikiPage,
        };
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new PersonaEngine();

//----------------------------------------------------------------------------------------------------------------------

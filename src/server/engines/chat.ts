//----------------------------------------------------------------------------------------------------------------------
// ChatEngine
//----------------------------------------------------------------------------------------------------------------------

// Models
import { Chat, ChatExternal } from '../../common/models/chats.js';
import { ValidationError } from '../../common/interfaces/validation.js';

// Utils
import { imageBufferToDataURI } from '../utils/avatars.js';

//----------------------------------------------------------------------------------------------------------------------

class ChatEngine
{
    async validateChat(chat : Chat) : Promise<ValidationError[]>
    {
        const validationErrors : ValidationError[] = [];

        if(chat.name && typeof chat.name !== 'string')
        {
            validationErrors.push({ message: 'Chat name must be a string or null.', field: 'name' });
        }

        if(chat.description && typeof chat.description !== 'string')
        {
            validationErrors.push({ message: 'Chat description must be a string or null.', field: 'description' });
        }

        if(chat.avatar && !chat.avatar.buffer)
        {
            validationErrors.push({ message: 'If specified, chat avatar must have a buffer.', field: 'avatar' });
        }

        if(chat.avatar && !chat.avatar.mime)
        {
            validationErrors.push({ message: 'If specified, chat avatar must have a mime type.', field: 'avatar' });
        }

        return validationErrors;
    }

    formatChatForExternal(chat : Chat) : ChatExternal
    {
        return {
            id: chat.id,
            name: chat.name,
            description: chat.description,
            members: [],
            avatar: chat.avatar ? imageBufferToDataURI(chat.avatar) : undefined,
        };
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new ChatEngine();

//----------------------------------------------------------------------------------------------------------------------

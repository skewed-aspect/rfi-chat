// ---------------------------------------------------------------------------------------------------------------------
// Conversation between Glyse and Whitney from right after they reconciled
// ---------------------------------------------------------------------------------------------------------------------

// Models
import { ChatImportRecord } from '../../common/models/import.js';

// Utils
import { getTimestamp } from '../utils/misc.js';

// ---------------------------------------------------------------------------------------------------------------------

export const exportedChat : ChatImportRecord = {
    chat: {
        details: {
            members: [
                {
                    username: 'wrenchGirl70',
                },
                {
                    username: 'AceHuntress62',
                },
            ],
        },
    },
    aliases: [],
    messages: [
        {
            author: 'AceHuntress62',
            content: 'Hey, Whit. Just wanted to say I had so much fun today. I can\'t believe I waited this long to talk to you. :roll_eyes:',
            createdAt: getTimestamp('2689-07-05T23:59:00+0000'),
        },
        {
            author: 'AceHuntress62',
            content: 'I\'ve literally had this chat pinned for forever. I\'m such a fucking idiot. :hocho::dizzy_face:',
            createdAt: getTimestamp('2689-07-05T23:59:18+0000'),
        },
        {
            author: 'AceHuntress62',
            content: 'Sorry, I didn\'t even ask if I could message you. My bad. I just assumed... but no, that was stupid. :upside_down_face:',
            createdAt: getTimestamp('2689-07-06T00:02:21+0000'),
        },
        {
            author: 'wrenchGirl70',
            content: 'oh my god, sorry',
            createdAt: getTimestamp('2689-07-06T02:25:47+0000'),
        },
        {
            author: 'wrenchGirl70',
            content: 'i was talking to anya and emmy. didnt mean to ignore u!',
            createdAt: getTimestamp('2689-07-06T02:25:52+0000'),
        },
        {
            author: 'wrenchGirl70',
            content: 'of course you can chat me!',
            createdAt: getTimestamp('2689-07-06T02:25:56+0000'),
        },
        {
            author: 'wrenchGirl70',
            content: 'ur not an idiot! :angry:',
            createdAt: getTimestamp('2689-07-06T02:26:07+0000'),
        },
        {
            author: 'wrenchGirl70',
            content: 'i had so much fun too. its scary, but im glad we did this. :sweat_smile:',
            createdAt: getTimestamp('2689-07-06T02:26:13+0000'),
        },
        {
            author: 'AceHuntress62',
            content: 'Shit, sorry. I passed out. Might\'ve started drinking wine until the room went dark. No worries, David spent the night. Still pissed at him, but he did have to deal with my drunk ass, sooo... guess we\'re cool.',
            createdAt: getTimestamp('2689-07-06T09:37:18+0000'),
        },
        {
            author: 'wrenchGirl70',
            content: 'omg!',
            createdAt: getTimestamp('2689-07-06T09:37:48+0000'),
        },
        {
            author: 'wrenchGirl70',
            content: 'yse, no!',
            createdAt: getTimestamp('2689-07-06T09:37:51+0000'),
        },
        {
            author: 'wrenchGirl70',
            content: 'why did you get drunk?!!!!!',
            createdAt: getTimestamp('2689-07-06T09:37:56+0000'),
        },
        {
            author: 'AceHuntress62',
            content: 'Uh...',
            createdAt: getTimestamp('2689-07-06T09:39:12+0000'),
        },
        {
            author: 'AceHuntress62',
            content: 'Nerves, I guess. Also, I dunno. Too many emotions. Had to get them out somehow. Drinking kinda does that? But like I said, I was safe. David was there.',
            createdAt: getTimestamp('2689-07-06T09:40:57+0000'),
        },
        {
            author: 'wrenchGirl70',
            content: 'ok...',
            createdAt: getTimestamp('2689-07-06T09:41:01+0000'),
        },
        {
            author: 'wrenchGirl70',
            content: 'just... don\'t hurt my friend, k?',
            createdAt: getTimestamp('2689-07-06T09:41:05+0000'),
        },
        {
            author: 'AceHuntress62',
            content: 'Your friend?',
            createdAt: getTimestamp('2689-07-06T09:42:25+0000'),
        },
        {
            author: 'wrenchGirl70',
            content: 'u',
            createdAt: getTimestamp('2689-07-06T09:42:27+0000'),
        },
        {
            author: 'AceHuntress62',
            content: '"u"?',
            createdAt: getTimestamp('2689-07-06T09:43:36+0000'),
        },
        {
            author: 'wrenchGirl70',
            content: 'YOU',
            createdAt: getTimestamp('2689-07-06T09:43:39+0000'),
        },
        {
            author: 'wrenchGirl70',
            content: 'dont make me type xtra.',
            createdAt: getTimestamp('2689-07-06T09:43:42+0000'),
        },
    ],
};

// ---------------------------------------------------------------------------------------------------------------------

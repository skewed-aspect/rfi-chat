// ---------------------------------------------------------------------------------------------------------------------
// importChats.ts
// ---------------------------------------------------------------------------------------------------------------------

import 'dotenv/config';
import configUtil from '@strata-js/util-config';
import logging from '@strata-js/util-logging';

// Models
import { Persona, PersonaAlias } from '../../common/models/personas.js';

// Chats
import { exportedChat as GlyseWhitneyChat } from './glyseWhitney.js';

// Resource Access
import * as chatRA from '../resource-access/chat.js';
import * as charAliasRA from '../resource-access/chatAliases.js';
import * as chatMessageRA from '../resource-access/chatMessages.js';
import * as personaRA from '../resource-access/personas.js';
import * as personaAliasRA from '../resource-access/personaAliases.js';

// Utils
import { shortID } from '../utils/misc.js';

// ---------------------------------------------------------------------------------------------------------------------

const chatRecords = [
    GlyseWhitneyChat,
];

async function importChats() : Promise<void>
{
    console.log('Importing chats...');

    // Load the configuration
    configUtil.load(`./config/local.yml`);

    // Set up logging
    logging.setConfig({ level: 'debug', prettyPrint: true });

    for(const record of chatRecords)
    {
        const chatID = shortID();
        const missingMembers : Partial<Persona>[] = [];
        const missingPersonaAliases : PersonaAlias[] = [];

        // We attempt to get the persona for every chat member, to ensure they exist
        for(const member of record.chat.details.members)
        {
            try
            {
                await personaRA.get(member.username);
            }
            catch(err)
            {
                missingMembers.push(member);
            }
        }

        // Add the personas, if they don't exist
        for(const member of missingMembers)
        {
            const newPersona : Persona = {
                fullName: member.fullName ?? '',
                username: member.username
            }

            await personaRA.create(newPersona);
        }

        // Look for any missing persona aliases
        for(const alias of record.aliases)
        {
            try
            {
                await personaAliasRA.get(alias.owner, alias.alias);
            }
            catch(err)
            {
                missingPersonaAliases.push(alias);
            }
        }

        // Add missing persona aliases
        for(const alias of missingPersonaAliases)
        {
            await personaAliasRA.create(alias);
        }

        // Add the chat
        await chatRA.create({
            id: chatID,
            name: record.chat.details.name,
            description: record.chat.details.description,
            avatar: record.chat.details.avatar,
            members: record.chat.details.members.map((member) => member.username),
        });

        // Add the chat alias
        if(record.chat.alias)
        {
            await charAliasRA.create({
                chatID,
                owner: record.chat.alias.owner,
                alias: record.chat.alias.alias,
                description: record.chat.alias.description,
            });
        }

        // Add chat messages
        for(const message of record.messages)
        {
            await chatMessageRA.create({
                id: shortID(),
                chatID,
                author: message.author,
                content: message.content,
                createdAt: message.createdAt,
                editedAt: message.editedAt,
            });
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------

importChats()
    .then(() =>
    {
        console.log('Chats imported successfully.');
        process.exit(0);
    })
    .catch((err) =>
    {
        console.error(err);
        process.exit(1);
    });

// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// import.ts
// ---------------------------------------------------------------------------------------------------------------------

import { PartialPersona, PersonaAlias } from './personas.js';
import { ChatAlias, Chat, ChatMessage } from './chats.js';

// ---------------------------------------------------------------------------------------------------------------------

export interface ChatDetails extends Omit<Chat, 'id' | 'members'>
{
    members : PartialPersona[];
}

export interface SimpleChatMessage extends Omit<ChatMessage, 'id' | 'chatID' | 'author'>
{
    author : string;
}

export interface ChatImportRecord
{
    chat : {
        details : ChatDetails
        alias ?: ChatAlias;
    };
    aliases : PersonaAlias[];
    messages : SimpleChatMessage[];
}

// ---------------------------------------------------------------------------------------------------------------------

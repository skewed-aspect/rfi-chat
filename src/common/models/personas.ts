// ---------------------------------------------------------------------------------------------------------------------
// personas.ts
// ---------------------------------------------------------------------------------------------------------------------

import { ImageBuffer } from '../interfaces/images.js';

// ---------------------------------------------------------------------------------------------------------------------

export interface Persona
{
    username : string;
    fullName : string;
    avatar ?: ImageBuffer
    wikiPage ?: string;
}

export interface PartialPersona extends Partial<Omit<Persona, 'username'>>
{
    username : string;
}

export interface PersonaExternal extends Omit<Persona, 'avatar'>
{
    avatar ?: string;
}

export interface PersonaAlias
{
    target : string;
    owner : string;
    alias : string;
}

// ---------------------------------------------------------------------------------------------------------------------
// DB Records
// ---------------------------------------------------------------------------------------------------------------------

export type PersonaAliasDBRecord = PersonaAlias;

export interface PersonaDBRecord
{
    username : string;
    full_name : string;
    avatar ?: Buffer | null;
    avatar_mime ?: string | null;
    wiki_page ?: string | null;
}

// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// Chat Models
// ---------------------------------------------------------------------------------------------------------------------

// Interfaces
import { ImageBuffer } from '../interfaces/images.js';

// Models
import { PersonaExternal } from './personas.js';

// ---------------------------------------------------------------------------------------------------------------------

export interface ChatMessage
{
    id : string | null;
    chatID : string;
    author : string;
    content : string;
    createdAt : number;
    editedAt ?: number | null;
}

export interface Chat
{
    id : string;
    name ?: string;
    description ?: string;
    avatar ?: ImageBuffer;
    members : string[];
}

export type PartialChat = Partial<Omit<Chat, 'id' | 'members'>>;

export interface ChatExternal extends Omit<Chat, 'members' | 'avatar'>
{
    members : PersonaExternal[];
    avatar ?: string;
}

export interface ChatAlias
{
    chatID : string;
    owner : string;
    alias : string;
    description ?: string;
}

// ---------------------------------------------------------------------------------------------------------------------
// DB Records
// ---------------------------------------------------------------------------------------------------------------------

export interface ChatAliasDBRecord extends Omit<ChatAlias, 'chatID'>
{
    chat_id : string;
}

export interface ChatDBRecord
{
    id : string;
    name ?: string | null;
    description ?: string;
    avatar ?: Buffer | null;
    avatar_mime ?: string | null;
}

export interface ChatMemberDBRecord
{
    id : number;
    chat_id : string;
    persona : string;
}

export interface ChatMessageDBRecord
{
    id : string;
    chat_id : string;
    author : string;
    content : string;
    created_at : number;
    edited_at ?: number | null;
}

// ---------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------

import 'dotenv/config';
import { ConfigUtil } from '@strata-js/util-config';
import type { Knex } from 'knex';

import { ServerConfig } from './src/common/interfaces/config';

//----------------------------------------------------------------------------------------------------------------------

const configUtil = new ConfigUtil();

const env = (process.env.ENVIRONMENT ?? 'local').toLowerCase();
configUtil.load(`./config/${ env }.yml`);

//----------------------------------------------------------------------------------------------------------------------

module.exports = {
    ...configUtil.get<ServerConfig>().database ?? {},
    migrations: {
        directory: './src/server/knex/migrations',
    },
    seeds: {
        directory: './src/server/knex/seeds',
    },
} satisfies Knex.Config;

//----------------------------------------------------------------------------------------------------------------------

# RFI Chat Generator

This is a site for generating fake chats between various characters, or group texts and embedding them. The whole 
point of this is to give an alternative form of recording events and descriptions of characters, rather than just 
prose or wiki entries.

This generator, unlike others that exist on the internet, is designed specifically to be integrated with the wiki.
